# Covid Testing Management System

## Development Environment
- [Dotnet Core SDK](https://dotnet.microsoft.com/download) v3.1.201

### Running the server
- `dotnet [watch] run`
  - [watch](https://docs.microsoft.com/en-us/aspnet/core/tutorials/dotnet-watch?view=aspnetcore-3.1)
