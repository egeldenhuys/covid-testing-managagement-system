using covid_testing_managagement_system.Data;
using covid_testing_managagement_system.Interfaces;
using covid_testing_managagement_system.Models;
using covid_testing_managagement_system.Models.Infermedica;
using covid_testing_managagement_system.Repositories;
using covid_testing_managagement_system.RepositoryInterfaces;
using covid_testing_managagement_system.Services;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.Data.SqlClient;

namespace covid_testing_managagement_system
{
    public class Startup
    {

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
            Connection = new SqlConnection(Configuration.GetValue<string>("ConnectionStrings:DefaultConnection"));
        }

        public IConfiguration Configuration { get; }
        public SqlConnection Connection { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddTransient<IUserStore<User>, UserStore>();
            services.AddTransient<IRoleStore<Role>, RoleStore>();
            services.AddIdentity<User, Role>();
            services.Configure<IdentityOptions>(options =>
            {
                // Increase security for prod.
                options.Password.RequireDigit = false;
                options.Password.RequireLowercase = false;
                options.Password.RequireNonAlphanumeric = false;
                options.Password.RequireUppercase = false;
                options.Password.RequiredLength = 1;
                options.Password.RequiredUniqueChars = 0;
            });

            // This is required to override the LoginPath redirect when the user is not authorized
            services.ConfigureApplicationCookie(options =>
                {
                    options.AccessDeniedPath = "/";
                    options.Cookie.Name = "MagicAuthCookie";
                    options.Cookie.HttpOnly = true;
                    options.ExpireTimeSpan = TimeSpan.FromMinutes(10);
                    options.LoginPath = "/Admin/Login";
                    options.ReturnUrlParameter = CookieAuthenticationDefaults.ReturnUrlParameter;
                    options.SlidingExpiration = true;
                });

            services.AddControllersWithViews();

            services.AddHttpClient();

            services.AddDistributedMemoryCache();

            services.AddSession(options =>
            {
                options.IdleTimeout = TimeSpan.FromMinutes(10);
                options.Cookie.HttpOnly = true;
                options.Cookie.IsEssential = true;
            });

            services.AddSingleton<IInfermedicaCredentials, InfermedicaCredentials>();
            services.AddSingleton<IInfermedicaService, InfermedicaService>();
            services.AddSingleton<IEmailCredentials, EmailCredentials>();
            services.AddSingleton<IEmailSender, EmailSender>();
            services.AddSingleton<IInterviewRepository, InterviewRepository>();
            services.AddSingleton<IInterviewEvidenceRepository, InterviewEvidenceRepository>();
            services.AddSingleton<IPatientRepository, PatientRepository>();
            services.AddSingleton<IProfessionalRepository, ProfessionalRepository>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseSession();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
