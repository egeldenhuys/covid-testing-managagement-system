﻿using covid_testing_managagement_system.Models.Admin;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace covid_testing_managagement_system.Interfaces
{
    public interface IInterviewEvidenceRepository
    {
        public Task<IEnumerable<InterviewEvidence>> GetInterviewEvidenceAsync(int interviewId);
        public Task<bool> AddInterviewEvidenceAsync(IEnumerable<InterviewEvidence> interviewEvidence);
    }
}
