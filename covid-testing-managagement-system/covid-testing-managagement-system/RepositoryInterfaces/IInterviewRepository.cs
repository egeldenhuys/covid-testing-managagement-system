﻿using covid_testing_managagement_system.Models.Admin;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace covid_testing_managagement_system.Interfaces
{
    public interface IInterviewRepository
    {
        public Task<Interview> GetInterview(int Id);
        public Task<IEnumerable<Interview>> GetInterviewsAsync();
        public Task<IEnumerable<Interview>> GetInterviewsByReviewedStatusAsync(bool reviewed);
        public Task<int> AddInterviewAsync(int PatientId);
        public Task<Interview> EditInterview(Interview interview);
    }
}
