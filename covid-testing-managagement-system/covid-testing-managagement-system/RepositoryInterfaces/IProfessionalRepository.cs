﻿using covid_testing_managagement_system.Models;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace covid_testing_managagement_system.RepositoryInterfaces
{
    public interface IProfessionalRepository
    {
        public Task<IEnumerable<Professional>> GetProfessionalsAsync();
        public Task<Professional> GetProfessionalByIdAsync(int id);
        public Task<Professional> GetProfessionalByEmailAsync(string email);
        public Task<Professional> GetProfessionalByRegistrationNumberAsync(int registartionNumber);
        public Task<Professional> GetProfessionalByIdNumberAsync(string IdNumber);
        public Task<Professional> AddProfessionalAsync(Professional professional, SqlConnection conn = null);
        public Task<IEnumerable<Professional>> GetProfessionalssByReviewedStatusAsync(bool reviewed);
        public Task UpdateProfessionalReviewedStatusAsync(int id, int reviewedStatus);
        public Task<int> GetUserIdFromProfessionalId(string ProfessionalId);
    }
}
