﻿using covid_testing_managagement_system.Models;
using System.Threading.Tasks;

namespace covid_testing_managagement_system.RepositoryInterfaces
{
    public interface IPatientRepository
    {
        public Task<Patient> AddPatientAsync(Patient patient);
        public Task<Patient> GetPatientByIdAsync(int id);
    }
}
