﻿using covid_testing_managagement_system.Models.Admin;
using System.Collections.Generic;

namespace covid_testing_managagement_system.Models
{
    public class DoctorDashboardViewModel
    {
        public List<Interview> Interviews { get; set; }
        public List<Patient> Patients { get; set; }
        public string ProfessionalName { get; set; }
        public bool IsNewPatients { get; set; }
    }
}
