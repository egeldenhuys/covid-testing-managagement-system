﻿using covid_testing_managagement_system.Models.Infermedica.Request;
using covid_testing_managagement_system.Models.Infermedica.Response;

namespace covid_testing_managagement_system.Models
{
    public class SymtomCheckerViewModel
    {
        public InfermedicaRequest Request { get; set; }
        public DiagnonsisResponse Response { get; set; }
    }
}
