using covid_testing_managagement_system.Models;
using covid_testing_managagement_system.Models.Admin;
using System.Collections.Generic;

namespace covid_testing_managagement_system.ViewModels.Admin
{
    public class PatientReportViewModel
    {
        public Patient patient { get; set; }
        public Interview interview { get; set; }
        public List<InterviewEvidence> SymptomsPresent { get; set; }
        public List<InterviewEvidence> SymptomsAbsent { get; set; }
        public List<InterviewEvidence> RiskFactorsPresent { get; set; }
        public List<InterviewEvidence> RiskFactorsAbsent { get; set; }
    }
}