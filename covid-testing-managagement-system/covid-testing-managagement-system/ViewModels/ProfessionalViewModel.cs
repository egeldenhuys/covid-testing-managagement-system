﻿using covid_testing_managagement_system.Models;

namespace covid_testing_managagement_system.ViewModels
{
    public class ProfessionalViewModel
    {
        public Professional Professional { get; set; }
        public LoginCredentials Login { get; set; }
    }
}
