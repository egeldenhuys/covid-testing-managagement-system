using MailKit.Net.Smtp;
using Microsoft.Extensions.Logging;
using MimeKit;
using System;
using System.Threading.Tasks;

namespace covid_testing_managagement_system.Services
{
    public class EmailSender : IEmailSender
    {
        private readonly IEmailCredentials _emailCredentials;
        private readonly ILogger<EmailSender> _logger;

        public EmailSender(ILogger<EmailSender> logger, IEmailCredentials emailCredentials)
        {
            this._emailCredentials = emailCredentials;
            this._logger = logger;
        }

        public async Task SendEmailAsync(string email, string subject, string message)
        {
            if (
                _emailCredentials.MailServer == null ||
                _emailCredentials.MailPort == 0 ||
                _emailCredentials.Password == null ||
                _emailCredentials.SenderAddress == null ||
                _emailCredentials.SenderName == null
            )
            {

                throw new Exception("Some user-secrets for the emailing service wasn't specified.");
            }

            var mimeMessage = new MimeMessage();

            mimeMessage.From.Add(new MailboxAddress(new string[] { _emailCredentials.SenderName }, _emailCredentials.SenderAddress));
            mimeMessage.To.Add(new MailboxAddress(email));
            mimeMessage.Subject = subject;
            mimeMessage.Body = new TextPart("plain")
            {
                Text = message
            };

            try
            {
                using var client = new SmtpClient();
                client.ServerCertificateValidationCallback = (s, c, h, e) => true;
                await client.ConnectAsync(_emailCredentials.MailServer, _emailCredentials.MailPort, true).ConfigureAwait(false);    // bool after port is for using SSL.
                await client.AuthenticateAsync(_emailCredentials.SenderAddress, _emailCredentials.Password).ConfigureAwait(false);
                await client.SendAsync(mimeMessage).ConfigureAwait(false);
                await client.DisconnectAsync(true).ConfigureAwait(false);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
        }

        // TODO: Will need to get FirstName, LastName, Email (User Model) and OutcomeGuid (Interview Model).
        // TODO: Might need to make emails env sensitive.
        // TODO: Don't forget to add the Recommendation/OutcomeGuid endpoint if we really need that.
        // TODO: Fix tabs showing in email.

        public async void SendTestingRecommendationEmail(string targetEmail)
        {
            string subject = "Testing Recommendation - Covid-19 Symptom Checker";
            
            string message =
                @"To Whom It May Concern,
                
                This is to inform you that you've been recommended for a Covid-19 test.
 
                Good luck with the test!

                Regards,

                Covid-19 Symptom Checker Team
                ";

            await this.SendEmailAsync(targetEmail, subject, message);
        }

        public async void SendTestingRejectionEmail(string targetEmail)
        {
            string subject = "Testing Recommendation - Covid-19 Symptom Checker";

            string message =
                @"To Whom It May Concern,

                Upon futher inspection of your symptoms, it was deemed that a testing recommendation was
                unwarranted. Should your symptoms worsen, don't hesitate to apply for another testing recommendation.

                Best of luck,

                Covid-19 Symptom Checker Team
                ";

            await this.SendEmailAsync(targetEmail, subject, message);
        }

        public async void SendRegistrationApprovedEmail(string targetEmail)
        {
            string subject = "Registration - Covid-19 Symptom Checker";

            string message =
                @"To Whom It May Concern,

                We're pleased to inform you that your registration was successful, you may now login using the below
                link:
                
                http://localhost:44320/Admin/Login

                Regards,

                Covid-19 Symptom Checker Team
                ";

            await this.SendEmailAsync(targetEmail, subject, message);
        }

        public async void SendRegistrationRejectedEmail(string targetEmail)
        {
            string subject = "Registration - Covid-19 Symptom Checker";

            string message =
                @"To Whom It May Concern,

                We're sorry to announce that your registration was unsuccessful. Should any further feedback be
                required, you can simply write to us and we'll get back to you as soon as possible. 

                Regards,

                Covid-19 Symptom Checker Team
                ";

            await this.SendEmailAsync(targetEmail, subject, message);
        }
    }
}