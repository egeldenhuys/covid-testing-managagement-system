﻿using covid_testing_managagement_system.Interfaces;
using covid_testing_managagement_system.Models.Admin;
using Dapper;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace covid_testing_managagement_system.Repositories
{
    public class InterviewEvidenceRepository : IInterviewEvidenceRepository
    {
        private readonly string ConnectionString;


        public InterviewEvidenceRepository(IConfiguration configuration)
        {
            ConnectionString = configuration.GetConnectionString("DefaultConnection");
        }

        public SqlConnection getConn()
        {
            return new SqlConnection(ConnectionString);
        }

        public async Task<bool> AddInterviewEvidenceAsync(IEnumerable<InterviewEvidence> Evidence)
        {
            string command = @"INSERT INTO[dbo].[InterviewEvidence]
                                ([InterviewId]
                              ,[EvidenceId]
                              ,[Choice])
                                VALUES
                               (@InterviewID
                               ,(SELECT TOP(1) Id FROM Evidence WHERE ApiId = @ApiId)
                               ,@Choice)";
            using SqlConnection conn = getConn();
            {
                conn.Open();
                using (var trans = conn.BeginTransaction())
                {
                    foreach (InterviewEvidence Evi in Evidence)
                    {
                        var catergories = new
                        {
                            InterviewID = Evi.InterviewId,
                            ApiId = Evi.ApiId,
                            Choice = Evi.Choice
                        };
                        await conn.ExecuteAsync(command, catergories, transaction: trans);
                    }
                    trans.Commit();
                }
            }

            return true;
        }


        public async Task<IEnumerable<InterviewEvidence>> GetInterviewEvidenceAsync(int interviewId)
        {
            string command = @"SELECT InterviewId, EvidenceId,Choice , [Name], CommonName, ApiId  FROM Interview 
                            INNER JOIN InterviewEvidence ON Interview.Id = InterviewEvidence.InterviewId
                            INNER JOIN Evidence ON InterviewEvidence.EvidenceId = Evidence.Id
                            WHERE Interview.Id = @InterviewId";
            return await getConn().QueryAsync<InterviewEvidence>(command, new { InterviewId = interviewId });

        }


        public async Task<IEnumerable<InterviewEvidence>> GetInterviewQuestionsAsync(int InterviewId)
        {
            string command = "SELECT [InterviewId] ,[EvidenceId] ,[Choice] FROM[dbo].[InterviewEvidence] WHERE [InterviewId] = @InterviewID";
            return await getConn().QueryAsync<InterviewEvidence>(command, new { InterviewID = InterviewId });
        }
    }
}
