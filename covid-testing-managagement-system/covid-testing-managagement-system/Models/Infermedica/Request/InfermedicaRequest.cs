﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace covid_testing_managagement_system.Models.Infermedica.Request
{
    public partial class InfermedicaRequest
    {

        public enum Gender
        {
            male,
            female
        }

        [JsonProperty("sex")]
        [JsonConverter(typeof(StringEnumConverter))]
        public Gender Sex { get; set; }

        [JsonProperty("age")]
        [Range(0, 100)]
        public int Age { get; set; }

        [JsonProperty("evidence")]
        public List<Evidence> Evidence { get; set; }

        public InfermedicaRequest()
        {
            Evidence = new List<Evidence>();
        }

        public void AddEvidence(Evidence e)
        {
            Evidence.Add(e);
        }

    }
}
