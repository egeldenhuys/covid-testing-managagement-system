﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace covid_testing_managagement_system.Models.Infermedica.Request
{
    public partial class Evidence
    {

        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("choice_id")]
        [JsonConverter(typeof(StringEnumConverter))]
        public Choice ChoiceId { get; set; }

        public Evidence()
        {

        }
    }
}
