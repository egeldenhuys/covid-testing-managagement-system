﻿using covid_testing_managagement_system.Services.Infermedica.Response;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.Collections.Generic;
using System.Net;

namespace covid_testing_managagement_system.Models.Infermedica.Response
{
    public class TriageResponse
    {
        public enum TriageResponseLevel
        {
            no_risk,
            self_monitoring,
            quarantine,
            isolation_call,
            call_doctor,
            isolation_ambulance
        }

        [JsonIgnore]
        public HttpStatusCode StatusCode { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("label")]
        public string Label { get; set; }

        [JsonProperty("serious")]
        public List<Observation> Serious { get; set; }

        [JsonProperty("triage_level")]
        [JsonConverter(typeof(StringEnumConverter))]
        public TriageResponseLevel TriageLevel { get; set; }

        public TriageResponse()
        {
        }
    }
}
