﻿using Newtonsoft.Json;

namespace covid_testing_managagement_system.Services.Infermedica.Response
{
    public class Observation
    {
        [JsonProperty("common_name")]
        public string CommonName { get; set; }

        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("is_emergency")]
        public bool IsEmergency { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }
    }
}
