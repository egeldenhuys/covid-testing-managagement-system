﻿using Newtonsoft.Json;

namespace covid_testing_managagement_system.Models.Infermedica.Response
{
    public class Item
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("explanation")]
        public string Explanation { get; set; }

        //[JsonProperty("choices")]
        //public List<Choice> Choices { get; set; }

        public bool Choice { get; set; }

        public Item()
        {
            //Choices = new List<Choice>();
            Choice = false;
        }
    }
}
