﻿namespace covid_testing_managagement_system.Models.Admin
{
    public class Interview
    {
        public int Id { get; set; }
        public int PatientId { get; set; }
        public bool Reviewed { get; set; }
        public bool Outcome { get; set; }
        public int ReviewerDoctorId { get; set; }
    }
}
