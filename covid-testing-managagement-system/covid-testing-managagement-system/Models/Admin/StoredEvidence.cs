namespace covid_testing_managagement_system.Models.Admin
{
    public class StoredEvidence
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string CommonName { get; set; }
        public string ApiId { get; set; }

        public StoredEvidence()
        {

        }
    }
}