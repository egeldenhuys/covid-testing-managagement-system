namespace covid_testing_managagement_system.Models
{
    public class LoginCredentials
    {
        public int ProfessionalId { get; set; }
        public string Password { get; set; }
        public bool isSuperUser { get; set; }
        public bool isApproved { get; set; }
        public bool Remember { get; set; }
    }
}