﻿using covid_testing_managagement_system.Interfaces;
using covid_testing_managagement_system.Models;
using covid_testing_managagement_system.Models.Admin;
using covid_testing_managagement_system.Models.Infermedica.Request;
using covid_testing_managagement_system.Models.Infermedica.Response;
using covid_testing_managagement_system.RepositoryInterfaces;
using covid_testing_managagement_system.Services;
using covid_testing_managagement_system.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static covid_testing_managagement_system.Models.Infermedica.Response.Question;

namespace covid_testing_managagement_system.Controllers
{
    public class SymtomCheckerController : Controller
    {

        private readonly string TRIAGE_MODEL_KEY = "_TriageViewModelData";
        private readonly ILogger<SymtomCheckerController> _logger;
        private readonly IInfermedicaService _infermedicaService;
        private readonly IPatientRepository _patientRepository;
        private readonly IInterviewRepository _interviewRepository;
        private readonly IInterviewEvidenceRepository _interviewEvidenceRepository;

        public SymtomCheckerController(ILogger<SymtomCheckerController> logger, IInfermedicaService infermedicaService, IPatientRepository patientRepository, IInterviewRepository interviewRepository, IInterviewEvidenceRepository interviewEvidenceRepository)
        {
            _infermedicaService = infermedicaService;
            _logger = logger;
            _patientRepository = patientRepository;
            _interviewRepository = interviewRepository;
            _interviewEvidenceRepository = interviewEvidenceRepository;
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult PatientInfo()
        {
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> PatientInfo(SymtomCheckerViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }

            model.Response = await _infermedicaService.SendDiagnosisRequestAsync(model.Request);
            if (model.Response.ShouldStop)
            {
                HttpContext.Session.Set<InfermedicaRequest>(TRIAGE_MODEL_KEY, model.Request);
                return RedirectToAction(nameof(Results));
            }

            if (model.Response.StatusCode != System.Net.HttpStatusCode.OK)
            {
                return StatusCode((int)model.Response.StatusCode);
            }

            string id = model.Response.Question.Items[0].Id;
            HttpContext.Session.Set(id, model);
            return RedirectToAction(nameof(Symtoms), new { Q_ID = id });
        }

        public ActionResult Symtoms(string Q_ID)
        {
            SymtomCheckerViewModel model = HttpContext.Session.Get<SymtomCheckerViewModel>(Q_ID);
            if (!ModelState.IsValid)
            {
                return View(nameof(Symtoms), model);
            }

            if (model == null)
            {
                _logger.LogError("No session data found for SymtomCheckerViewModel");
                return StatusCode(500);
            }

            string id = model.Response.Question.Items[0].Id;
            HttpContext.Session.Set(id, model);
            return View(nameof(Symtoms), model.Response.Question);
        }

        [HttpPost]
        public async Task<ActionResult> Symtoms(Question question)
        {
            SymtomCheckerViewModel model = HttpContext.Session.Get<SymtomCheckerViewModel>(question.Items[0].Id);
            if (!ModelState.IsValid)
            {
                return View(nameof(Symtoms), model);
            }

            if (model == null)
            {
                _logger.LogError("No session data found for SymtomCheckerViewModel");
                return StatusCode(500);
            }

            if (question.Type != QuestionType.group_single)
            {
                foreach (Item item in question.Items)
                {
                    Models.Infermedica.Request.Evidence evidence = new Models.Infermedica.Request.Evidence
                    {
                        Id = item.Id
                    };
                    evidence.ChoiceId = item.Choice ? Models.Infermedica.Choice.present : Models.Infermedica.Choice.absent;
                    model.Request.AddEvidence(evidence);
                }
            }
            else
            {
                foreach (Item item in question.Items)
                {
                    if (item.Choice)
                    {
                        Models.Infermedica.Request.Evidence evidence = new Models.Infermedica.Request.Evidence
                        {
                            Id = item.Id
                        };
                        evidence.ChoiceId = item.Choice ? Models.Infermedica.Choice.present : Models.Infermedica.Choice.absent;
                        model.Request.AddEvidence(evidence);
                        break;
                    }
                }
            }

            model.Response = await _infermedicaService.SendDiagnosisRequestAsync(model.Request);
            if (model.Response.ShouldStop)
            {
                TriageViewModel triageViewModel = new TriageViewModel
                {
                    Request = model.Request
                };
                HttpContext.Session.Set(TRIAGE_MODEL_KEY, triageViewModel);
                return RedirectToAction(nameof(Results));
            }

            if (model.Response.StatusCode != System.Net.HttpStatusCode.OK)
            {
                return StatusCode((int)model.Response.StatusCode);
            }

            string id = model.Response.Question.Items[0].Id;
            HttpContext.Session.Set(id, model);
            return RedirectToAction(nameof(Symtoms), new { Q_ID = id });
        }

        public async Task<ActionResult> Results()
        {
            TriageViewModel triageViewModel = HttpContext.Session.Get<TriageViewModel>(TRIAGE_MODEL_KEY);
            if (triageViewModel == null)
            {
                _logger.LogWarning("Page was called with no triageViewModel view data");
                return BadRequest();
            }

            triageViewModel.Response = await _infermedicaService.SendTriageRequestAsync(triageViewModel.Request);

            if (triageViewModel.Response.StatusCode != System.Net.HttpStatusCode.OK)
            {
                return StatusCode((int)triageViewModel.Response.StatusCode);
            }

            HttpContext.Session.Set(TRIAGE_MODEL_KEY, triageViewModel);
            return View(triageViewModel.Response);
        }

        public IActionResult Apply()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> ApplyAsync(PatientViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }

            TriageViewModel triageViewModel = HttpContext.Session.Get<TriageViewModel>(TRIAGE_MODEL_KEY);
            if (triageViewModel == null)
            {
                _logger.LogWarning("Page was called with no triageViewModel view data");
                return BadRequest();
            }

            if (triageViewModel.Request == null)
            {
                _logger.LogWarning("Page was called with no Triage Request data");
                return BadRequest();
            }

            if (triageViewModel.Response == null)
            {
                _logger.LogWarning("Page was called with no Triage Response data");
                return BadRequest();
            }

            Patient patient = new Patient
            {
                Age = triageViewModel.Request.Age,
                DateApplied = DateTime.Now,
                Email = model.Email,
                FirstName = model.FirstName,
                IdNumber = model.IdNumber,
                LastName = model.LastName
            };
            if (triageViewModel.Request.Sex == InfermedicaRequest.Gender.male)
            {
                patient.Sex = true;
            }
            else
            {
                patient.Sex = false;
            }

            patient = await _patientRepository.AddPatientAsync(patient);
            int interviewId = await _interviewRepository.AddInterviewAsync(patient.Id);
            IEnumerable<InterviewEvidence> interviewEvidence = ConvertEvidence(interviewId, triageViewModel.Request.Evidence);
            await _interviewEvidenceRepository.AddInterviewEvidenceAsync(interviewEvidence);

            return RedirectToAction(nameof(Applied));
        }

        private IEnumerable<InterviewEvidence> ConvertEvidence(int interviewId, List<Models.Infermedica.Request.Evidence> evidence)
        {
            return evidence.Select(e => new InterviewEvidence { InterviewId = interviewId, ApiId = e.Id, Choice = e.ChoiceId == Models.Infermedica.Choice.present });
        }

        public IActionResult Applied()
        {
            return View();
        }
    }
}