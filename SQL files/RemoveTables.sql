USE Covid19ManagementSystem 
GO

DROP TABLE [InterviewEvidence]
DROP TABLE [Evidence]
DROP TABLE [Interview]
DROP TABLE [Professional]
DROP TABLE [Patient]
DROP TABLE [PersonalInfo]
DROP TABLE [PersonType]
DROP TABLE [Country]
DROP TABLE [Speciality]
DROP TABLE [UserRole]
DROP TABLE [Role]
DROP TABLE [User]
