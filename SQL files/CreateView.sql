CREATE VIEW PatientSymptomsView AS
SELECT Patient.FirstName, Patient.LastName, Patient.Sex, Patient.DOB, [dbo].[Symptoms].Symptom, Patient.Description
FROM [dbo].[Patient] INNER JOIN [dbo].[PatientSymptoms] ON [dbo].[Patient].ID = [dbo].[PatientSymptoms].PatientID
INNER JOIN [dbo].[Symptoms] ON [dbo].[PatientSymptoms].SymptomID = [dbo].[Symptoms].ID